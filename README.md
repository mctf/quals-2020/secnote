# Writeup

To sovle this task you shoud:
 - know the SQL and what is SQL injection

Solution:
 - The vulnerable functionality was in `/Find_notes` endpoint
To exploit it to see all of the notes (private and not private) you should send this payload to server
`\" or 1=1 -- "\`
`http://secnote.mctf.online/Find_notes?q=\" or 1=1 -- "\`
Yoy can see the vulnerable SQL query in routes.py at the `search_note()` function.