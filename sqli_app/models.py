from contextlib import contextmanager

from sqlalchemy import create_engine, orm
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('mysql+pymysql://root:gika6wesd32vd1s@sqli_db:3306/Secnote?charset=utf8mb4', pool_recycle=3600)
Base = declarative_base()


def init_sessionmaker():
    """Создает глобальную сессию."""
    global Session
    Session = orm.sessionmaker(bind=engine)


@contextmanager
def session_scope():
    """Удобная обертка для открытия безопасного сессий БД."""
    session = Session()
    try:
        yield session
        session.commit()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()
